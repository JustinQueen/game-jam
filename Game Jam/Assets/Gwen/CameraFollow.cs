﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject attachment;
    private float transitionTime;
    // Start is called before the first frame update
    void Start()
    {
        transitionTime = 0;
    }
    private void Update()
    {
        if (!attachment.GetComponent<Movement>().spinout)
        {
            transform.rotation = attachment.transform.rotation;
            if (Vector3.Distance(attachment.transform.position, transform.position) > 1)
            {
                transitionTime += Time.deltaTime / 6.0f;
                transform.position = Vector3.Lerp(transform.position, attachment.transform.position, transitionTime);
            }
            else
            {
                transitionTime = 0;
                transform.position = attachment.transform.position;
            }
        }
    }
}
