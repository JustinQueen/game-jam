﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movement : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed, rotateSpeed, frictionVar, angleFrictionVar, deadZone, rotDeadZone, rotSpeedCap, speedCap, rotSpeedSlowdown, upDownDeadZone;
    [SerializeField]
    private GameObject leftArm, rightArm;
    private Vector3 initialPos;
    private Quaternion initialRot;
    private Rigidbody rb;
    [SerializeField]
    private List<Vector3> leftArf, rightArf;
    private OwO paw;
    private ChargeState chargeState;
    [SerializeField]
    private float chargeMaxCooldown, chargeForce, maxChargeTime, chargeSpeedCap;
    [SerializeField]
    private GameObject dashMeter;
    [SerializeField]
    private Color dashMeterCol, dashCooldownCol;
    private float chargeCurrentCooldown, currentChargeTime;
    public bool spinout;
    [SerializeField]
    private float spinoutMax, spinoutSpeed;
    [SerializeField]
    private int spinoutPenalty;
    private float spinoutCurrent;
    public int playerIndex;
    [SerializeField]
    private AudioClip whimper, screech, crash, bark;
    [SerializeField]
    private AudioSource audioSource;

    public enum OwO
    {
        Uwp,
        Neutwal,
        Down,
        Gwab
    }
    public enum ChargeState
    {
        Charge,
        Uncharge,
        Cooldown
    }

    // Start is called before the first frame update
    void Start()
    {
        initialPos = transform.position;
        initialRot = transform.rotation;
        rb = gameObject.GetComponent<Rigidbody>();
        paw = OwO.Gwab;
        chargeState = ChargeState.Uncharge;
        chargeCurrentCooldown = 0;
        currentChargeTime = 0;
        spinout = false;
        spinoutCurrent = 0;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (transform.localEulerAngles.x != 0)
        {
            transform.localEulerAngles = new Vector3(0.0f, transform.localEulerAngles.y, 0.0f);
        }
        if (transform.localEulerAngles.z != 0)
        {
            transform.localEulerAngles = new Vector3(0.0f, transform.localEulerAngles.y, 0.0f);
        }
        if (transform.position.y < -10)
        {
            Reset();
        }
        //if not spinning out
        if (!spinout)
        {
            float upAxe = Input.GetAxis("Vertical_" + playerIndex);
            float horAxe = Input.GetAxis("Horizontal_" + playerIndex);
            //Forward
            rb.AddForce((transform.forward * moveSpeed * upAxe));
            //Left/right
            rb.angularVelocity += new Vector3(0.0f, rotateSpeed * horAxe, 0.0f);
            //Caps
            if (rb.angularVelocity[1] > 1.0f || rb.angularVelocity[1] < -1.0f)
            {
                rb.velocity = new Vector3(rb.velocity[0] / rotSpeedSlowdown, rb.velocity[1], rb.velocity[2] / rotSpeedSlowdown);
            }
            //Friction
            if (horAxe < rotDeadZone && horAxe > -rotDeadZone)
            {
                audioSource.clip = screech;
                audioSource.Play();
                rb.angularVelocity /= angleFrictionVar;
            }
            if (upAxe < deadZone && upAxe > -deadZone)
            {
                rb.velocity = new Vector3(rb.velocity.x / frictionVar, rb.velocity.y, rb.velocity.z / frictionVar);
            }
            //Clamp
            if (chargeState != ChargeState.Charge)
            {
                rb.velocity = new Vector3(Mathf.Clamp(rb.velocity[0], -speedCap, speedCap), Mathf.Clamp(rb.velocity[1], -speedCap, speedCap), Mathf.Clamp(rb.velocity[2], -speedCap, speedCap));
                rb.angularVelocity = new Vector3(Mathf.Clamp(rb.angularVelocity[0], -rotSpeedCap, rotSpeedCap), Mathf.Clamp(rb.angularVelocity[1], -rotSpeedCap, rotSpeedCap), Mathf.Clamp(rb.angularVelocity[2], -rotSpeedCap, rotSpeedCap));
            }
            else
            {
                rb.velocity = new Vector3(Mathf.Clamp(rb.velocity[0], -chargeSpeedCap, chargeSpeedCap), Mathf.Clamp(rb.velocity[1], -chargeSpeedCap, chargeSpeedCap), Mathf.Clamp(rb.velocity[2], -chargeSpeedCap, chargeSpeedCap));
            }
            //Puttin out the paw real far owo
            float fireLeft = Input.GetAxis("Fire1_" + playerIndex);
            float fireRight = Input.GetAxis("Fire2_" + playerIndex);
            float upDown = Input.GetAxis("Vertical2_" + playerIndex);
            //Stick out Left
            if (fireLeft > 0.0f && !(fireRight > 0.0f))
            {
                //Up
                if (upDown > upDownDeadZone)
                {
                    paw = OwO.Uwp;
                    PawOut(true);
                }
                //Down
                else if (upDown < -upDownDeadZone)
                {
                    paw = OwO.Down;
                    PawOut(true);
                }
                //Neutral
                else
                {
                    paw = OwO.Neutwal;
                    PawOut(true);
                }
            }
            //FireRight
            else if (fireRight > 0.0f && !(fireLeft > 0.0f))
            {
                //Up
                if (upDown > upDownDeadZone)
                {
                    paw = OwO.Uwp;
                    PawOut(false);
                }
                //Down
                else if (upDown < -upDownDeadZone)
                {
                    paw = OwO.Down;
                    PawOut(false);
                }
                //Neutral
                else
                {
                    paw = OwO.Neutwal;
                    PawOut(false);
                }
            }
            else
            {
                paw = OwO.Gwab;
                PawOut(false);
                PawOut(true);
            }
            //Charge
            bool charge = Input.GetButton("Fire3_" + playerIndex);
            if ((chargeState == ChargeState.Uncharge) && charge || chargeState == ChargeState.Charge)
            {
                rb.AddForce((transform.forward * chargeForce));
                audioSource.clip = bark;
                audioSource.Play();
                chargeState = ChargeState.Charge;
            }
            if (chargeState == ChargeState.Cooldown)
            {
                chargeCurrentCooldown += Time.deltaTime;
                dashMeter.transform.localScale = new Vector3(chargeCurrentCooldown / chargeMaxCooldown, dashMeter.transform.localScale.y, dashMeter.transform.localScale.z);
                if (chargeCurrentCooldown >= chargeMaxCooldown)
                {
                    chargeState = ChargeState.Uncharge;
                    chargeCurrentCooldown = 0;
                    dashMeter.GetComponent<Image>().color = dashMeterCol;
                }
            }
            if (chargeState == ChargeState.Charge)
            {
                currentChargeTime += Time.deltaTime;
                dashMeter.transform.localScale = new Vector3((1.0f - currentChargeTime / maxChargeTime), dashMeter.transform.localScale.y, dashMeter.transform.localScale.z);
                //Change some UI ELEMENT
                if (currentChargeTime >= maxChargeTime)
                {
                    chargeState = ChargeState.Cooldown;
                    dashMeter.GetComponent<Image>().color = dashCooldownCol;
                    currentChargeTime = 0;
                }
            }
        }
        else
        {
            spinoutCurrent += Time.deltaTime;
            if(spinoutCurrent > spinoutMax)
            {
                spinout = false;
                spinoutCurrent = 0;
                return;
            }
            rb.angularVelocity += new Vector3(0.0f, spinoutSpeed, 0.0f);
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (!spinout && collision.gameObject.CompareTag("Arm") && collision.gameObject != gameObject && chargeState != ChargeState.Charge)
        {
            SpinOut();
        }
        if(collision.gameObject.CompareTag("Enemy"))
        {
            audioSource.clip = whimper;
            audioSource.Play();
            PlayerChase.EnemyType enemyType = collision.gameObject.GetComponent<PlayerChase>().enemyType;
            if(enemyType == PlayerChase.EnemyType.Manager)
            {
                Destroy(collision.gameObject);
                SpinOut();
                Reset();
            }
            else if(enemyType == PlayerChase.EnemyType.Police)
            {
                Destroy(collision.gameObject);
                SpinOut();
            }
        }
    }
    // true = left, false = right
    public void PawOut(bool wPaw)
    {
        if (wPaw)
        {
            if (!Vector3.Equals(leftArm.transform.eulerAngles, leftArf[(int)paw]))
            {
                leftArm.transform.localEulerAngles = leftArf[(int)paw];
            }
        }
        else
        {
            if (!Vector3.Equals(rightArm.transform.eulerAngles, rightArf[(int)paw]))
            {
                rightArm.transform.localEulerAngles = rightArf[(int)paw];
            }
        }
    }

    public void SpinOut()
    {
        audioSource.clip = crash;
        audioSource.Play();
        spinout = true;
        GetComponent<ScoreManager>().ChangeScore(-spinoutPenalty);
    }
    public void Reset()
    {
        transform.position = initialPos;
        transform.rotation = initialRot;
        rb.velocity = Vector3.zero;
    }
}
