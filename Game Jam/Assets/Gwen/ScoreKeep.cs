﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreKeep : MonoBehaviour
{
    [SerializeField]
    int pointValue;
    float despawnTime = 0.1f;
    float timer;
    [SerializeField]
    bool hit;
    private void Start()
    {
        hit = false;
        timer = 0;
    }
    private void Update()
    {
        if (hit)
        {
            timer += Time.deltaTime;
            if (timer > despawnTime)
            {
                Destroy(gameObject);
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Floor") && !hit)
        {
            hit = true;
        }
        if (other.gameObject.CompareTag("Arm") && !hit)
        {
            other.gameObject.GetComponentInParent<ScoreManager>().ChangeScore(pointValue);
            hit = true;
        }
    }
}
