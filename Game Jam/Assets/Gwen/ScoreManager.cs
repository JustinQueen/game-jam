﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreManager : MonoBehaviour
{
    [SerializeField]
    int score;
    public Text UI;
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
    }
    private void Update()
    {
        if (UI.text != score.ToString())
        {
            UI.text = score.ToString() + " $";
        }
    }
    public void ChangeScore(int scoreToAdd)
    {
        if (score + scoreToAdd > 0)
        {
            score += scoreToAdd;
        }
        else
        {
            score = 0;
        }
    }
    public int GetScore()
    {
        return score;
    }
}
