﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="Events", menuName = "Events/New Event", order = 1)]

public class Events : ScriptableObject
{
    public int index; 
    public MonoBehaviour associatedScript;
    
}
