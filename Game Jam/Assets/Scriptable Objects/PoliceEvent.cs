﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceEvent : MonoBehaviour
{
    [SerializeField]
    GameObject[] officerSpawn;
    [SerializeField]
    GameObject officerPrefab;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < officerSpawn.Length; i++)
        {
        Instantiate(officerPrefab, officerSpawn[i].transform.position, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
