﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerChase : MonoBehaviour
{
    int speed;
    NavMeshAgent navMesh;
    public PlayerManager playerManager;
    int randPlayer;
    public EnemyType enemyType;

    public enum EnemyType
    {
        Police,
        Manager
    }
    // Start is called before the first frame update
    void Start()
    {
        navMesh = GetComponent<NavMeshAgent>();
        if (enemyType == EnemyType.Manager)
        {
            randPlayer = Random.Range(0, playerManager.playerList.Count);
        }
        else
        {
            int max = playerManager.playerList[0].GetComponent<ScoreManager>().GetScore();
            int indMax = 0;
            for(int i = 1; i < playerManager.playerList.Count; i++)
            {
                if(playerManager.playerList[i].GetComponent<ScoreManager>().GetScore() > max)
                {
                    max = playerManager.playerList[i].GetComponent<ScoreManager>().GetScore();
                    indMax = i;
                }
            }
            randPlayer = indMax;
        }
    }

    // Update is called once per frame
    void Update()
    {
        navMesh.SetDestination(playerManager.playerList[randPlayer].transform.position);
        if(navMesh.destination == transform.position)
        {
            Destroy(gameObject);
        }
    }
    
}
