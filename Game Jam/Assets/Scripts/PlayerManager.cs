﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{
    public List<GameObject> playerList;
    [SerializeField]
    private List<GameObject> enemies;
    [SerializeField]
    private List<Vector3> enemyLocations;
    [SerializeField]
    private List<float> enemyEventTimes;
    [SerializeField]
    private GameObject everythingObject;
    [SerializeField]
    private float initialTime;
    [SerializeField]
    private List<Text> timeText;
    [SerializeField]
    private Text winText;
    [SerializeField]
    private RawImage winPopup;
    [SerializeField]
    private Camera winCamera;
    private float timeS;
    private bool endGame;
    // Start is called before the first frame update
    void Start()
    {
        timeS = initialTime * 60;
        endGame = false;
    }

    // Update is called once per frame
    void Update()
    {
        timeS -= Time.deltaTime;
        if (endGame)
        {
            bool input = Input.GetButton("Fire1_0");
            if (input)
            {
                SceneManager.LoadScene("Start Screen");
            }
        }
        else
        {
            if (timeS < 0)
            {
                EndGame();
            }
            else
            {
                for(int i = 0; i < enemyEventTimes.Count; i++)
                {
                    if(enemyEventTimes[i] == (int)timeS)
                    {
                        SpawnEnemy();
                        enemyEventTimes.RemoveAt(i);
                        i--;
                    }
                }
                int minutes, seconds;
                minutes = (int)timeS / 60;
                seconds = (int)timeS % 60;
                string timeMinutes, timeSeconds;
                if (minutes < 10)
                {
                    timeMinutes = "0" + minutes;
                }
                else
                {
                    timeMinutes = minutes.ToString();
                }
                if (seconds < 10)
                {
                    timeSeconds = "0" + seconds;
                }
                else
                {
                    timeSeconds = seconds.ToString();
                }
                for (int i = 0; i < timeText.Count; i++)
                {
                    timeText[i].text = timeMinutes + ":" + timeSeconds;
                }
            }
        }
    }
    public void EndGame()
    {
        everythingObject.SetActive(false);
        winPopup.gameObject.SetActive(true);
        winText.gameObject.SetActive(true);
        winCamera.gameObject.SetActive(true);
        int max = playerList[0].GetComponentInChildren<ScoreManager>().GetScore();
        int maxInd = 0;
        for (int i = 1; i < playerList.Count; i++)
        {
            if (playerList[i].GetComponentInChildren<ScoreManager>().GetScore() > max)
            {
                max = playerList[i].GetComponentInChildren<ScoreManager>().GetScore();
                maxInd = i;
            }
        }
        List<int> winInd = new List<int>();
        winInd.Add(maxInd);
        for (int i = maxInd + 1; i < playerList.Count; i++)
        {
            if (playerList[i].GetComponentInChildren<ScoreManager>().GetScore() == playerList[maxInd].GetComponentInChildren<ScoreManager>().GetScore())
            {
                winInd.Add(i);
            }
        }
        string addStatement = " has won";
        string addWinText = "Player";
        if (winInd.Count > 1)
        {
            addStatement = " have tied";
            addWinText += "s ";

        }
        else
        {
            addWinText += " ";
        }
        for (int i = 0; i < winInd.Count; i++)
        {
            addWinText += (i + 1).ToString();
            if(i < winInd.Count - 1)
            {
                addWinText += ", ";
            }
        }
        addWinText += addStatement + " with a cost of " + playerList[winInd[0]].GetComponentInChildren<ScoreManager>().GetScore() + "$!";
        winText.text = addWinText;
        endGame = true;
    }

    public void SpawnEnemy()
    {
        int number = Random.RandomRange(1, enemyLocations.Count);
        List<Vector3> locations = new List<Vector3>(number);
        for (int i = 0; i < number; i++)
        {
            locations.Add(enemyLocations[i]);
        }
        for (int i = 0; i < locations.Count; i++)
        {
            int enemyType = Random.RandomRange(0, enemies.Count);
            GameObject enemy = Instantiate(enemies[enemyType], everythingObject.transform);
            enemy.GetComponent<PlayerChase>().playerManager = gameObject.GetComponent<PlayerManager>();
            enemy.transform.position = locations[i];
        }
    }
}
