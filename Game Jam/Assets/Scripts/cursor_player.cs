﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class cursor_player : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public int index;
    public bool state1 = true;
    public bool state2 = false;
    public bool state3 = false;
    public bool state4 = false;
    public bool menu_counter_v = true;
    public bool menu_counter_h = true;

    // Update is called once per frame
    void Update()
    {
        float v_axis = Input.GetAxis("Vertical_0");
        float h_axis = Input.GetAxis("Horizontal_0");


        //moving down
        if(v_axis < 0 && menu_counter_v == true && menu_counter_h == true)
        {
            if (state1 == true)
            {

                transform.position = new Vector3(transform.position.x, transform.position.y - 235, transform.position.z);
                state3 = true;
                state1 = false;

            }
            else if (state2 == true)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y - 235, transform.position.z);
                state4 = true;
                state2 = false;
            }
            else if (state3 == true)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + 235, transform.position.z);
                state3 = false;
                state1 = true;

            }
            else if(state4 == true)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + 235, transform.position.z);
                state4 = false;
                state2 = true;
            }
            

            menu_counter_v = false;
        }
        //moving up
        if(v_axis > 0 && menu_counter_v == true && menu_counter_h == true)
        {
            if (state1 == true)
            {

                transform.position = new Vector3(transform.position.x, transform.position.y - 235, transform.position.z);
                state3 = true;
                state1 = false;

            }
            else if (state2 == true)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y - 235, transform.position.z);
                state4 = true;
                state2 = false;
            }
            else if (state3 == true)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + 235, transform.position.z);
                state3 = false;
                state1 = true;

            }
            else if (state4 == true)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + 235, transform.position.z);
                state4 = false;
                state2 = true;
            }


            menu_counter_v = false;
        }


        //moving left
        if (h_axis < 0 && menu_counter_h == true && menu_counter_v == true)
        {
   
            if (state2 == true)
            {
                transform.position = new Vector3(transform.position.x - 450, transform.position.y, transform.position.z);
                state1 = true;
                state2 = false;
            }
         
            else if (state4 == true)
            {
                transform.position = new Vector3(transform.position.x - 450, transform.position.y, transform.position.z);
                state4 = false;
                state3 = true;
            }


            menu_counter_h = false;
        }


        //moving right
        if (h_axis > 0 && menu_counter_h == true && menu_counter_v == true)
        {
            if (state1 == true)
            {

                transform.position = new Vector3(transform.position.x + 450, transform.position.y, transform.position.z);
                state2 = true;
                state1 = false;

            }
           
            else if (state3 == true)
            {
                transform.position = new Vector3(transform.position.x + 450, transform.position.y, transform.position.z);
                state3 = false;
                state4 = true;

            }
           


            menu_counter_h = false;
        }


        //when user presses A

        if (Input.GetButton("Fire1_0"))
        {
            if(state1 == true)
            {
                SceneManager.LoadScene("Level1P", LoadSceneMode.Single);
            }
            else if(state2 == true)
            {
                SceneManager.LoadScene("Level2P", LoadSceneMode.Single);
            }
            else if (state3 == true)
            {
                SceneManager.LoadScene("Level3P", LoadSceneMode.Single);
            }
            else if (state4 == true)
            {
                SceneManager.LoadScene("Level4P", LoadSceneMode.Single);
            }
        }


            if (v_axis == 0)
        {
            menu_counter_v = true;
        }
        if(h_axis == 0)
        {
            menu_counter_h = true;
        }

    }
}
