﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class cursorscript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public int index;
    public bool state1 = true;
    public bool state2 = false;
    public bool state3 = false;
    public bool menu_counter = true;
    
    // Update is called once per frame
    void Update()
    {
        
        float v_axis = Input.GetAxis("Vertical_0");

       
        if(v_axis < 0 && menu_counter == true)
        {
            if (state1 == true)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y - 85, transform.position.z);
                state1 = false;
                state2 = true;
            }
            else if(state2 == true)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y - 85, transform.position.z);
                state2 = false;
                state3 = true;
            }
            else if (state3 == true)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + 170, transform.position.z);
                state3 = false;
                state1 = true;
            }
            menu_counter = false;
        }

        if(v_axis > 0 && menu_counter == true)
        {
            if (state1 == true)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y - 170, transform.position.z);
                state1 = false;
                state3 = true;
            }
            else if (state2 == true)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + 85, transform.position.z);
                state2 = false;
                state1 = true;
            }
            else if (state3 == true)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + 85, transform.position.z);
                state3 = false;
                state2 = true;
            }
            menu_counter = false;
        }

        if (v_axis == 0)
        {
            menu_counter = true;
        }

        if (Input.GetButton("Fire1_0"))
        {
            if (state1 == true)
            {
                //go to main game
                SceneManager.LoadScene("PlayerSelect", LoadSceneMode.Single);
            }
            else if (state2 == true)
            {
                //go to credits scene
                SceneManager.LoadScene("CreditsScene", LoadSceneMode.Single);
            }
            else if (state3 == true)
            {
                //ends the game
                Application.Quit();

            }
        }




    }
}
